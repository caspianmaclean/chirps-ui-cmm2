# chirps-ui-cmm2

A little program for the Colour Maximite 2 that makes sound effects like you might have on 1980's computers like the BBC Microcomputer or ZX Spectrum. It lets you change the sounds using the keyboard.